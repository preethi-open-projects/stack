
#ifndef STACK_H_
#define STACK_H_

#include<stdint.h>
#include<stdbool.h>

typedef struct _stack_info_t{
	int top;
	uint8_t size;
	uint8_t length;
	uint8_t* array;

	uint32_t init_magic_number;
}stack_t;

void stack_init(stack_t *stack,uint8_t *array,uint8_t size);
bool stack_push(stack_t *stack,uint8_t data);
bool stack_pop(stack_t *stack,uint8_t *data);
void stack_print_details(stack_t *stack);


#endif /* STACK_H_ */
