#include<stdio.h>
#include<stdint.h>
#include"stack.h"
#include"stdbool.h"
#include<stdlib.h>

#define STACK_CAPACITY 1

static stack_t _stack ;
static uint8_t _stack_arr[STACK_CAPACITY];

static void _add_data(uint8_t data);
static void _remove_data(void);
///////////////////////////////////////////Public Functions
int main(void)
{

	stack_init(&_stack,_stack_arr,STACK_CAPACITY);
	stack_print_details(&_stack);


	_add_data(10);
	_add_data(11);
	stack_print_details(&_stack);

	_remove_data();
	stack_print_details(&_stack);

	_add_data(20);
	_add_data(30);
	_add_data(40);
	stack_print_details(&_stack);

	_remove_data();
	_remove_data();
	stack_print_details(&_stack);

	return 0;
}

//////////////////////////////////////////Private Functions
static void _add_data(uint8_t data)
{
	bool result;
	result=stack_push(&_stack, data);
	if(result==true)
		printf("Adding data to stack %d\r\n",data);
	else
		printf("Failed to add data\r\n");
}

static void _remove_data(void)
{
	bool result;
	uint8_t data;

	result=stack_pop(&_stack,&data);
	if(result==true)
		printf("Succesfully removed data from stack\r\n");
	else
		printf("Failed to remove data\r\n");
}


