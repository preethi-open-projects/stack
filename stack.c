#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>
#include"stack.h"


#define INIT_MAGIC_NUMBER 0XDEADBEEF
//////////////////////////////////////////////Public Functions
void stack_init(stack_t *stack,uint8_t *array,uint8_t size)
{
	if(!stack || !array)
		return;

	if(stack->init_magic_number==INIT_MAGIC_NUMBER)
		return;

	stack->array=array;

	stack->top=-1;

	stack->size=size;
	stack->length=0;

	stack->init_magic_number=INIT_MAGIC_NUMBER;
}

bool stack_push(stack_t *stack,uint8_t data)
{
	if(!stack)
		return false;

	if(stack->init_magic_number!=INIT_MAGIC_NUMBER)
		return false;

	if(stack->length >= stack->size)
		return false;

	stack->top++;
	stack->array[stack->top]=data;
	stack->length++;
	return true;
}

bool stack_pop(stack_t *stack,uint8_t *data)
{
	if(!stack || !data)
		return false;

	if(stack->init_magic_number!=INIT_MAGIC_NUMBER)
		return false;

	if(stack->length==0)
		return false;

	*data=stack->array[stack->top];
	stack->array[stack->top]=0;
	stack->top--;
	stack->length--;
	return true;
}

void stack_print_details(stack_t* stack)
{
	printf("length:%d (size %d)\r\n",stack->length,stack->size);

	for(uint8_t i=0;i<stack->length;i++)
	{
		printf("%d ",stack->array[i]);
	}
	printf("\n");
}
